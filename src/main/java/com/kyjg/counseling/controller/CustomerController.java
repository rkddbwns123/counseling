package com.kyjg.counseling.controller;

import com.kyjg.counseling.model.CustomerRequest;
import com.kyjg.counseling.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody CustomerRequest request) {
        customerService.setCustomer(request.getName(), request.getPhone());

        return "OK";
    }
}
